describe user('jenkins-exporter') do
  it { should exist }
end

describe group('jenkins-exporter') do
  it { should exist }
end

describe file('/opt/jenkins-exporter') do
 its('type') { should eq :directory }
 it { should be_directory }
end

describe file('/opt/jenkins-exporter/requirements.txt') do
 it { should exist }
 it { should be_owned_by 'jenkins-exporter' }
end

describe file('/opt/jenkins-exporter/jenkins_exporter.py') do
 it { should exist }
 it { should be_owned_by 'jenkins-exporter' }
end
